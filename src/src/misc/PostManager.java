package src.misc;

import src.database.PostgresSQLDBC;
import src.classes.Post;

public class PostManager {
	PostgresSQLDBC driver;
	public PostManager(){
		this.driver = new PostgresSQLDBC();
	}
	
	public void createPost(int id, String title, String body, int likes, int userId){
		Post post = new Post(id, title, body, likes, userId);
		driver.createPost(post);
	}
}
