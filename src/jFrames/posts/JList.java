package jFrames.posts;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class JList extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JList frame = new JList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JList() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtpnAllPosts = new JTextPane();
		txtpnAllPosts.setBounds(167, 12, 59, 21);
		contentPane.add(txtpnAllPosts);
		txtpnAllPosts.setText("All posts");
		
		JPanel panel = new JPanel();
		panel.setBounds(42, 67, 356, 221);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JTextArea bodyInput = new JTextArea();
		bodyInput.setBounds(42, 51, 302, 86);
		panel.add(bodyInput);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setBounds(227, 149, 117, 25);
		panel.add(btnNewButton);
		
		JTextArea titleInput = new JTextArea();
		titleInput.setBounds(42, 12, 302, 27);
		panel.add(titleInput);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 3, 3);
		contentPane.add(scrollPane);
	}
}
